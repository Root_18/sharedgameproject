package main;

import java.awt.Graphics;
import java.util.LinkedList;

import entities.Entity;

public class EntityHandler {
    
    // A linked list is a list, where all the objects have a pointer to the objects next to it. We're using that because we can
    private LinkedList<Entity> entities = new LinkedList<Entity>(); // This is the list of our entities
    private Main main; // So far not needed, keeping it just in case

    public EntityHandler(Main main) { // Creating the entity handler, taking in main just in case we need it later
        this.main = main;
    }
    
    public void addEntity(Entity e) { // Adds an entity to the list
        entities.add(e);
    }
    
    public void removeEntity(Entity e) { // Removes an entity from the list
        entities.remove(e);
    }
    
    public void tick() { // Calls the tick() method on all of the entities
        for (Entity entity : entities) {
            entity.tick();
        }
    }
    
    public void render(Graphics g) { // Calls the render() method on all of the entities
        for (Entity entity : entities) {
            entity.render(g);
        }
    }


}