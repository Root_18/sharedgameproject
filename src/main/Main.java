package main;
//moi
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import entities.Player;
import input.KeyboardInput;
import sound.MusicPlayer;


// Testing stuff, sorry about this comment: v1

public class Main extends Canvas implements Runnable { // Canvas - so that we can draw on it, Runnable - so we can run it
	
	private static final long serialVersionUID = 1L; // <---- Stuff that eclipse likes to add, don't worry about it
		
	private Thread thread; // The main thread, threads make multitasking work. We're probably only going to use this one, no more. Kinda useless
	private boolean running = false; // Is the program running or not, default false
	
	private EntityHandler entityHandler; // It's the entity - handler wow
	
	public static void main(String[] args) { // Just some things to make the program dynamic
		new Main();
	}
	
	
	
	public Main() { // Main method - program starts here. **********************  PROGRAM START  **********************
		
		new Window(Variables.WINDOWWIDTH, Variables.WINDOWHEIGHT, Variables.WINDOW_TITLE, this); // Creates the new window with values in the 'Variables' class and using this
		
		this.addKeyListener(new KeyboardInput()); // Adds a key listener to this, our canvas, with the KeyboardInput class as the parameter
		
		entityHandler = new EntityHandler(this); // Creating our entity handler with this class being the main
		
		Player player = new Player(100, 100); // Creates our player at (100, 100)
		entityHandler.addEntity(player); // Adds our player to the list
		
		new MusicPlayer().loopMusic(); // Starts the music
		start(); // Starts the program
	}

	public void run() { // Our main loop gets called here
		int ticks = Variables.TICKS; // How many ticks we have is how many times tick() is called per second
		double ns = 1000000000.00 / ticks; // ns is the time in nanoseconds between ticks
		
		long lastTime = System.nanoTime(); // We'll be measuring the time between runs, and lastTime will tell us how much time has passed
		
		double delta = 0.00; // The total time that has passed since last tick() call
		
		while (running) { // Loops through until the program closes
			long currentTime = System.nanoTime(); // Gets the current time
			delta += (currentTime - lastTime); // Add the time that passed since last check
			
			if (delta >= ns) { // If enough time has passed, tick()
				tick();
				delta -= ns; // Reduce the tick time from delta
			}
			
			if (running) // Checks if the program is running, just in case
				render(); // tick() gets called 60 times per second, render() has no cap
			
			lastTime = currentTime; // Sets the new lastTime for next run
			
		}
	}
	
	public void start() { // Starts the program 
		running = true;
		
		thread = new Thread(this); // Makes the thread with this class
		thread.start(); 		   // Calls run() with that thread
	}
	
	public void stop() { // Stops the program
		running = false; // Stops the main game loop
		try {
			thread.join(); // Closes the thread, might not work and might cause an error, which we deal with below
		} catch (InterruptedException e) {
			e.printStackTrace(); // Deals with error, crashes program and gives us information about the crash
		}
	}
	
	public void tick() { // Tick gets called 60 times per second, use this for logic
		entityHandler.tick(); // Ticks all the entities (So far just the player)
	}

	public void render() { // Render gets called as many times per second as the machine lets us, use this for drawing only
		BufferStrategy bs = this.getBufferStrategy(); // Get our buffer strategy
		
		if (bs == null) { // Check if it exists, if not, create it and wait for next render call
			this.createBufferStrategy(3); // Create strategy with 3 buffers, should negate screen tearing
			return; // this.createBufferStrategy(3) takes effect next frame, return out of the method
		}
		
		Graphics g = bs.getDrawGraphics(); // Gets the graphics we can draw with
		
		g.setColor(Color.BLACK); // Sets the colour to back
		g.fillRect(0, 0, Variables.WINDOWWIDTH, Variables.WINDOWHEIGHT); // Draws background
		
		
		
		// This block of code renders the white box
		g.setColor(Color.WHITE); // Sets colour to white
		
		int x = Variables.WINDOWWIDTH / 2 - Variables.WIDTH / 2; // Sets x (where the left side of the box is)
		int y = Variables.WINDOWHEIGHT / 2 - Variables.HEIGHT / 2; // Sets y (at what height the left top corner is)
		
		g.drawRect(x, y, Variables.WIDTH, Variables.HEIGHT); // Draws the rectangle
		g.translate(x, y); // Makes it so, that (0, 0) is now the box's left top corner and not the screen's
		
		
		
		
		
		
		renderWithGraphics(g); // Another render method called
		
		g.dispose(); // Gets rid of graphics for another frame
		bs.show(); // Shows the newest frame
		
	}
	
	public void renderWithGraphics(Graphics g) { // You can draw anything here!
		entityHandler.render(g); // Renders all entities (So far just the player)
	}
	
}
