package main;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Window extends Canvas { // The window object - creates the window we can draw objects on.
	
	private static final long serialVersionUID = 1L; // <-- More stuff to make eclipse happy
	
	public JFrame frame; // Keeping an instance of the frame
	
	public Window(int width, int height, String title, Main main) { // The window needs width, height, a title, and the main class
		JFrame frame = new JFrame(title); // Creating the frame
		
		Dimension d = new Dimension(width, height); // Creating the dimensions for the frame
		frame.setPreferredSize(d); // Settings the frames size to the dimension
		frame.setMinimumSize(d);
		frame.setMaximumSize(d);
		
		frame.setResizable(false); // So the frame can't be resized
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Closes the window when we press X
		
		frame.add(main); // Adding main to the frame
		
		frame.pack(); // Makes stuff work
		
		frame.setVisible(true); // Makes the frame visible
		
	}
	
}
