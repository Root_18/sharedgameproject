package main;

public class Variables {
	
	
	// Contains final - unchangeable variables that we want to use
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	
	public static final int WINDOWWIDTH = 1920;
	public static final int WINDOWHEIGHT = 1080;
	
	public static final int TICKS = 60;
	
	public static final int PLAYER_SPEED = 5;
	
	public static final int PLAYER_SIZE = 32;
		
	
	public static final String WINDOW_TITLE = "The game window";
	
}
