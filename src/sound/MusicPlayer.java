package sound;

import java.io.File;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

public class MusicPlayer {
	
	
	/*
	 *  Just made this class, don't expect any comments, I just slapped this together and it looks really ugly my guys
	 * 
	 * 
	 * 
	 * 
	 */
	
	private static boolean running = false;
	private static Thread thread;
	
	public void playMusic() {
		try {
		    File musicFile = new File("src/resources/songSmooth.wav");
		    AudioInputStream stream;
		    AudioFormat format;
		    DataLine.Info info;
		    Clip clip;

		    stream = AudioSystem.getAudioInputStream(musicFile);
		    format = stream.getFormat();
		    info = new DataLine.Info(Clip.class, format);
		    clip = (Clip) AudioSystem.getLine(info);
		    clip.open(stream);
		    clip.start();
		}
		catch (Exception e) {
		    e.printStackTrace();
		}
	}
	
	public void loopMusic() {
		try {
		    File musicFile = new File("src/resources/songSmooth.wav");
		    AudioInputStream stream;
		    AudioFormat format;
		    DataLine.Info info;
		    Clip clip;

		    stream = AudioSystem.getAudioInputStream(musicFile);
		    format = stream.getFormat();
		    info = new DataLine.Info(Clip.class, format);
		    clip = (Clip) AudioSystem.getLine(info);
		    clip.open(stream);
		    clip.loop(99);
		    clip.start();
		}
		catch (Exception e) {
		    e.printStackTrace();
		}
	}
	
	
}
