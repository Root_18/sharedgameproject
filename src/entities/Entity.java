package entities;

import java.awt.Graphics;

public abstract class Entity { // This is abstract, so you can't create an entity by itself, but you can create
							   // something, that inherits entity, such as a player object
	
	// These are private, so they can't be accessed by other classes, but the getters and setters at the bottom can
	
	private int x = 0, y = 0; // Just some default values for the entity position
	private int velx = 0, vely = 0; // Some default values for the entity velocity
	private int id = 0;		 // A default value for the entity id, every type of entity has a different one
	
	public Entity(int x, int y, int id) { // While creating an entity, you have to know the spawn x and y coordinates and the id
		this.x = x; // Takes the local x, y and id values and stores them in the class x, y and id values
		this.y = y;
		this.id = id;
	}
	
	public void tick() { // Tick adds the value of velocityX to X and velocityY to Y to move the object by its velocity each tick
		x += velx;
		y += vely;
	}
	
	public void render(Graphics g) { // Render does nothing for now, as every entity may look different
		
	}
	
	// Getters below - used to get the value of variables
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

	public int getVelX() {
		return velx;
	}
	
	public int getVelY() {
		return vely;
	}
	
	public int getID() {
		return id;
	}
	
	
	// Setters below - used to set the values of variables
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setVelX(int velx) {
		this.velx = velx;
	}
	
	public void setVelY(int vely) {
		this.vely = vely;
	}
	
	
	
}
