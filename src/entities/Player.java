package entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import input.KeyboardInput;
import main.Variables;
import util.Tools;

public class Player extends Entity { // Extends entity a.k.a the Player is an Entity.
	
	private BufferedImage img;
	
	public Player(int x, int y) { // Creating a new player
		super(x, y, 0); // Creating the super entity a.k.a the entity that is the player if that makes sense
		loadTexture();  // Loads the player texture 
	}
	
	public void tick() {
		super.tick(); // Does the velocity calculations on the entity side
		
		
		movementUpdate(); // Updates the direction the player is going / Checks key input
		
		clampPosition(); // Prevents movement outside the box
	}
	
	private void movementUpdate() {
		int speed = Variables.PLAYER_SPEED; // Sets our speed to the speed defined in Variables class
		
		int newVelocityX = 0; // The new velocities that we will set later in the method
		int newVelocityY = 0;
		
		if (KeyboardInput.FORWARD) {
			newVelocityY -= speed; // If we are pressing W, remove 5 from our velocity
		}
		
		if (KeyboardInput.BACKWARD) {
			newVelocityY += speed;	// If we are pressing S, add 5 to our velocity
		}
		
		if (KeyboardInput.LEFT) {
			newVelocityX -= speed; // If we are pressing A, remove 5 from our velocity
		}
		
		if (KeyboardInput.RIGHT) {
			newVelocityX += speed; // If we are pressing D, add 5 to our velocity
		}
		
		super.setVelX(newVelocityX); // Sets the new velocity
		super.setVelY(newVelocityY);
	}
	
	private void clampPosition() {
		int size = Variables.PLAYER_SIZE; // Getting some variables from the Variables class and bringing them local
		int width = Variables.WIDTH;
		int height = Variables.HEIGHT;
		
		int x = super.getX(); // Gets the x
		int y = super.getY(); // Gets the y
		
		if (x < 0) x = 0; // If x is less than 0 (out of bounds) set x to 0 (in bounds)
		if (x + size > width) x = (width - size); // If x is more than the width (out of bounds) set x to the width (in bounds)
		
		if (y < 0) y = 0; // If y is less than 0 (out of bounds) set y to 0 (in bounds)
		if (y + size > height) y = height - size;
		
		super.setX(x);
		super.setY(y);
	}
	
	private void loadTexture() {
		try {
			int size = Variables.PLAYER_SIZE; // Gets the player size
			BufferedImage image = ImageIO.read(new FileInputStream(new File("src/resources/testingicon.png"))); // Gets the player image
			img = Tools.resize(image, size, size); // Resizes the player image to 32x32px
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void render(Graphics g) {
		/* int size = Variables.PLAYER_SIZE; // Gets the player size from Variables class
		*
		* g.setColor(Color.YELLOW); // Sets the colour to yellow
		* g.fillRect(super.getX(), super.getY(), size, size); // Fills in a rectangle at (x, y) with side lengths of 32 pixels
		*
		*/
		
		g.drawImage(img, super.getX(), super.getY(), null);
	}
	
}
