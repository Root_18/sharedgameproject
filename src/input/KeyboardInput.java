package input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardInput implements KeyListener {
    
	public static boolean FORWARD = false,
			BACKWARD = false,
			LEFT = false,
			RIGHT = false;
	
	
	
	
    public void keyTyped(KeyEvent e) {} // We don't care about this, we'll just leave it empty
    
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_W) {
        	FORWARD = true;
        } else if (e.getKeyCode() == KeyEvent.VK_S) {
        	BACKWARD = true;
        } else if (e.getKeyCode() == KeyEvent.VK_A) {
        	LEFT = true;
        } else if (e.getKeyCode() == KeyEvent.VK_D) {
        	RIGHT = true;
        }
    }
    
    public void keyReleased(KeyEvent e) {
    	if (e.getKeyCode() == KeyEvent.VK_W) {
        	FORWARD = false;
        } else if (e.getKeyCode() == KeyEvent.VK_S) {
        	BACKWARD = false;
        } else if (e.getKeyCode() == KeyEvent.VK_A) {
        	LEFT = false;
        } else if (e.getKeyCode() == KeyEvent.VK_D) {
        	RIGHT = false;
        }
    }
    
}
    